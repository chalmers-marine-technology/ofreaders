# This file is part of ofreaders
# (c) Timofey Mukha
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from os.path import join
import os
import numpy as np
import re
import h5py

__all__ = ["Forces", "collect_forces_over_time", "collect_moments_over_time"]



class Forces:
    """Output from the forces function object stored as a numpy arrays.

    Use the read() method to read the data from a file. The object will
    correspond to a single file, so if you want te read in both forces
    and moments you will need to Forces objects.

    Attributes
    ----------
    data : ndarray
        The whole forces or moments data, 3 cols for total, 3 for
        pressure, 3 for viscous.
    tot : ndarray
        The total forces or moments.
    p : ndarray
        The pressure forces or moments.
    nu : ndarray
        The viscous forces or moments.
    times : ndarray
        The time value for which data are available.

    """
    def __init__(self, times, data):
        self.times = times
        self.data = data

        self.tot = data[:, :3]
        self.p = data[:, 3:6]
        self.nu = data[:, 6:]

    @classmethod
    def read(cls, path):
        """
        Read the data from a file.

        Parameters
        ----------
        path : string
            The location of the file with the forces or moments.

        """
        # Regexp pattern for a float
        floatPattern = r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?"

        # Pattern to match "(float float float)"
        patternVector = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")


        times = []
        data = []
        with open(path, 'r') as f:
            for line in f:
                lineData = []
                # skip header
                if line[0] == "#":
                    continue
                else:
                    times.append(float(re.match(floatPattern, line).group()))
                    for element in patternVector.finditer(line):
                        split = element.group()[1:-1].split()
                        lineData += split
                data.append(np.array(lineData).astype(np.float32))

        data = np.stack(np.array(data), axis=0)

        return cls(np.array(times), data)

    def save_to_hdf5(self, filename):
        """
        Save the forces into an hdf5 file.

        The hdf5 will contain the datasets p, nu, and tot.
        All of these correspond to the respective attributes of the class.

        Parameters
        ----------
        filename : string
            The name of the file including extension. Alternatively, the whole path to the file.

        """
        f = h5py.File(filename, 'w')

        f.create_dataset("p", data=self.p, compression="gzip")
        f.create_dataset("nu", data=self.nu, compression="gzip")
        f.create_dataset("tot", data=self.tot, compression="gzip")
        f.create_dataset("times", data=self.times, compression="gzip")

        f.close()


def _collect_over_time(path, datatype):
    timeDirs = np.array(os.listdir(path))
    timesFloat = [float(i) for i in timeDirs]
    timeDirs = timeDirs[np.argsort(timesFloat)]

    if timeDirs.size == 0:
        raise IOError

    data = []
    times = []
    for time in timeDirs:
        if os.path.isfile(join(path, time, datatype+".dat")):
            dataI = Forces.read(join(path, time, datatype+".dat"))
            times.append(dataI.times)
            data.append(dataI.data)

    if len(times) == 0:
        raise IOError("No data found")
    else:
        times = np.concatenate(times)
        times, indForces = np.unique(times, return_index=True)
        data = np.row_stack(data)[indForces, :]
    return Forces(times, data)




def collect_forces_over_time(path):
    """
    Collect the forces data over several time directories.

    Repeating time-values will be removed.

    Parameters
    ----------
    path : string
        The location of the folder holding the forces data.

    Returns
    -------
    Forces
        Contains the forces data.

    """
    return _collect_over_time(path, "force")

def collect_moments_over_time(path):
    """
    Collect the moments data over several time directories.

    Repeating time-values will be removed.

    Parameters
    ----------
    path : string
        The location of the folder holding the moments data.

    Returns
    -------
    Forces
        Contains the moments data.

    """
    return _collect_over_time(path, "moment")
