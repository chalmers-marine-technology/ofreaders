# This file is part of ofreaders
# (c) Timofey Mukha
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from os.path import join
import os
import numpy as np
import re
import h5py

__all__ = ["Probes", "collect_probed_field_over_time"]


class Probes:
    """Probe data stored as a numpy array, plus additional atributes.

    Use the read() method to read the data from a file.

    Attributes
    ----------
    n : int
        The number of probes
    type : {"scalar", "vector", "symmTensor", "tensor"}
        The type of field that has been probed.
    locations : ndarray
        n by 3 array holding the spatial locations of the probes.
    data : ndarray
        The probed data. 2D for a scalar field otherwise 3d, with the third dimension storing
        the components of the vector/symmTensor/tensor at a given time for a given probe.
    times : ndarray
        The time value for which probes are available.

    """

    def __init__(self, n, type, locations, data, times):
        self. n = n
        self.type = type
        self.locations = locations
        self.data = data
        self.times = times

    @classmethod
    def read(cls, path, verbose=False):
        """
        Read the data from a file.

        Parameters
        ----------
        path : string
            The location of the file with the probes.
        verbose : bool
            Default is False, setting to True will produce extra output info upon reading.

        """
        # Regexp pattern for a float
        floatPattern = r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?"

        # Pattern to match "# Probe integer (float float float)"
        patternLocation = re.compile(r"# Probe \d* \(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern for the header row
        patternHeaders = re.compile(r"# *Probe *\d *\d")

        # Pattern for the # Time row
        patternTime = re.compile(r"# *Time")

        # Pattern to match "(float float float)"
        patternVector = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float)"
        patternSymmTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                               floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float float float float)"
        patternTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                           floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                           floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Get  number of probes, their location and determine the type field
        nProbes = 0
        locations = []
        # Guess for a vector
        patternType = patternVector
        type = "vector"

        with open(path, 'r') as f:
            for line in f:
                matchLocation = patternLocation.match(line)
                matchHeaders = patternHeaders.match(line)
                matchTime = patternTime.match(line)
                matchVector = patternVector.search(line)
                matchSymmTensor = patternSymmTensor.search(line)
                matchTensor = patternTensor.search(line)
                if matchLocation:
                    nProbes += 1
                    split = matchLocation.group().split()
                    locations.append(np.array([float(split[3][1:]), float(split[4]), float(split[5][:-1])]))
                elif matchHeaders or matchTime:
                    continue
                elif matchTensor:
                    type ="tensor"
                    patternType = patternTensor
                    nCols = 9
                    break
                elif matchSymmTensor:
                    type ="symmTensor"
                    patternType = patternSymmTensor
                    nCols = 6
                    break
                elif matchVector:
                    nCols = 3
                    break
                else:
                    type = "scalar"
                    nCols = 1
                    # Dont need a pattern
                    break

        if verbose:
            print("File contains", nProbes, "probes. The field type is", type + ".")

        with open(path, 'r') as f:
             nLines = 0
             for line in f:
                 if line[0] == "#":
                     continue
                 else:
                     nLines +=1
        if verbose:
            print("File contains", nLines, "timesteps")

        locations = np.row_stack(locations)

        if type == "scalar":
            # can use genfromtxt immediately
            data = np.genfromtxt(path, comments="#")
            times = data[:, 0]
            data = data[:, 1:]
        else:
            data = np.zeros((nLines, nProbes, nCols), dtype=np.float32)
            times = np.zeros((nLines, 1))
            with open(path, 'r') as f:
                i = 0
                for line in f:
                    if verbose and np.remainder(i, 100) == 0:
                        print("Read-in", i, "of", nLines, "lines")
                    # skip the auxillary lines
                    if line[0] == "#":
                        continue
                    else:
                        times[i] = float(re.match(r" *" + floatPattern, line).group())
                        j = 0
                        for element in patternType.finditer(line):
                            split = element.group()[1:-1].split()
                            data[i, j, :] = np.array(split).astype(np.float32)
                            j += 1
                    i += 1

        return cls(nProbes, type, locations, data, times)

    @staticmethod
    def type_and_pattern(path):
        # Regexp pattern for a float
        floatPattern = r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?"

        # Pattern to match "# Probe integer (float float float)"
        patternLocation = re.compile(r"# Probe \d* \(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern for the header row
        patternHeaders = re.compile(r"# *Probe *\d *\d")

        # Pattern for the # Time row
        patternTime = re.compile(r"# *Time")

        # Pattern to match "(float float float)"
        patternVector = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float)"
        patternSymmTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                       floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float float float float)"
        patternTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                   floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                   floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Get  number of probes, their location and determine the type field
        n = 0
        locations = []
        # Guess for a vector
        patternType = patternVector
        type = "vector"

        with open(path, 'r') as f:
            for line in f:
                matchLocation = patternLocation.match(line)
                matchHeaders = patternHeaders.match(line)
                matchTime = patternTime.match(line)
                matchVector = patternVector.search(line)
                matchSymmTensor = patternSymmTensor.search(line)
                matchTensor = patternTensor.search(line)
                if matchLocation:
                    n += 1
                    split = matchLocation.group().split()
                    locations.append(np.array([float(split[3][1:]), float(split[4]), float(split[5][:-1])]))
                elif matchHeaders or matchTime:
                    continue
                elif matchTensor:
                    type ="tensor"
                    patternType = patternTensor
                    break
                elif matchSymmTensor:
                    type ="symmTensor"
                    patternType = patternSymmTensor
                    break
                elif matchVector:
                    break
                else:
                    break

        return type, patternType, np.row_stack(locations)

    def save_to_hdf5(self, filename):
        """
        Save the probe data into an hdf5 file.

        The hdf5 will contain attributed n and type, and datasets locations, times and data.
        All of these correspond to the respective attributes of the class.

        Parameters
        ----------
        filename : string
            The name of the file including extension. Alternatively, the whole path to the file.

        """
        f = h5py.File(filename, 'w')

        f.attrs['n'] = self.n
        f.attrs['type'] = self.type

        f.create_dataset("locations", data=self.locations)
        f.create_dataset("times", data=self.times)
        f.create_dataset("data", data=self.data, compression="gzip")

        f.close()


def collect_probed_field_over_time(path, field):
    """
    Collect the probe data for a given field over several time directories.

    Assumes probe locations are the same for all times. Repeating time-values will be removed.

    Parameters
    ----------
    path : string
        The location of the folder holding the probe data. Typically
        postProcessing/nameOfFunctionObject.
    field : string
        The name of a field

    Returns
    -------
    Probes
        Contains the probe data.

    """
    timeDirs = np.array(os.listdir(path))
    timesFloat = [float(i) for i in timeDirs]
    timeDirs = timeDirs[np.argsort(timesFloat)]

    if timeDirs.size == 0:
        raise IOError

    probeData = []
    probeTimes = []
    for time in timeDirs:
        if os.path.isfile(join(path, time, field)):
            probeDataI = Probes.read(join(path, time, field))
            probeTimes.append(probeDataI.times)
            probeData.append(probeDataI.data)
            # The below should be the same for all time directories
            locations = probeDataI.locations
            n = probeDataI.n
            type = probeDataI.type

    if len(probeTimes) == 0:
        raise IOError("Field "+field+" not found.")
    else:
        probeTimes = np.concatenate(probeTimes)
        times, ind = np.unique(probeTimes, return_index=True)
        data = np.row_stack(probeData)[ind, :]

    return Probes(n, type, locations, data, times)

