# This file is part of ofreaders
# (c) Timofey Mukha
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from os.path import join
import os
import numpy as np
import re
import h5py

__all__ = ["FieldValue", "collect_fieldvalue_over_time"]


class FieldValue:
    """FieldValue data stored as a numpy array, plus additional atributes.

    Use the read() method to read the data from a file.

    Attributes
    ----------
    type : {"scalar", "vector", "symmTensor", "tensor"}
        The type of field that has been probed.
    data : ndarray
        The probed data. 2D for a scalar field otherwise 3d, with the third dimension storing
        the components of the vector/symmTensor/tensor at a given time for a given probe.
    times : ndarray
        The time value for which probes are available.

    """

    def __init__(self, type, data, times):
        self.type = type
        self.data = data
        self.times = times

    @classmethod
    def read(cls, path, verbose=False):
        """
        Read the data from a file.

        Parameters
        ----------
        path : string
            The location of the file with the probes.
        verbose : bool
            Default is False, setting to True will produce extra output info upon reading.

        """
        # Regexp pattern for a float
        floatPattern = r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?"

        # Pattern to match "(float float float)"
        patternVector = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float)"
        patternSymmTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                               floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        # Pattern to match "(float float float float float float float float float)"
        patternTensor = re.compile(r"\(" + floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                           floatPattern + r" " + floatPattern + r" " + floatPattern + r" " +
                                           floatPattern + r" " + floatPattern + r" " + floatPattern + r"\)")

        patternType = patternVector
        type = "vector"

        with open(path, 'r') as f:
            for line in f:
                matchVector = patternVector.search(line)
                matchSymmTensor = patternSymmTensor.search(line)
                matchTensor = patternTensor.search(line)
                if line[0] is "#":
                    continue
                elif matchTensor:
                    type ="tensor"
                    patternType = patternTensor
                    nCols = 9
                    break
                elif matchSymmTensor:
                    type ="symmTensor"
                    patternType = patternSymmTensor
                    nCols = 6
                    break
                elif matchVector:
                    nCols = 3
                    break
                else:
                    type = "scalar"
                    nCols = 1
                    # Dont need a pattern
                    break

        if verbose:
            print("The field type is", type + ".")

        with open(path, 'r') as f:
             nLines = 0
             for line in f:
                 if line[0] == "#":
                     continue
                 else:
                     nLines +=1
        if verbose:
            print("File contains", nLines, "timesteps")

        if type == "scalar":
            # can use genfromtxt immediately
            data = np.genfromtxt(path, comments="#")
            times = data[:, 0]
            data = data[:, 1:]
        else:
            data = np.zeros((nLines, nCols), dtype=np.float32)
            times = np.zeros((nLines, 1))

            with open(path, 'r') as f:
                i = 0
                for line in f:
                    if verbose and np.remainder(i, 100) == 0 and i > 0:
                        print("Read-in", i, "of", nLines, "lines")
                    # skip the auxillary lines
                    if line[0] == "#":
                        continue
                    else:
                        times[i] = float(re.match(floatPattern, line).group())
                        element = patternType.search(line)[0]
                        split = element[1:-1].split()
                        data[i, :] = np.array(split).astype(np.float32)
                    i += 1

        return cls(type, data, times)


    def save_to_hdf5(self, filename):
        """
        Save the data into an hdf5 file.

        The hdf5 will contain attributes n and type, and datasets times and data.
        All of these correspond to the respective attributes of the class.

        Parameters
        ----------
        filename : string
            The name of the file including extension. Alternatively, the whole path to the file.

        """
        f = h5py.File(filename, 'w')

        f.attrs['type'] = self.type

        f.create_dataset("times", data=self.times)
        f.create_dataset("data", data=self.data, compression="gzip")

        f.close()


def collect_fieldvalue_over_time(path, fieldType, verbose=False):
    """
    Collect the fieldvalue data for a given field over several time directories.

    Repeating time-values will be removed.

    Parameters
    ----------
    path : string
        The location of the folder holding the probe data. Typically
        postProcessing/nameOfFunctionObject.
    fieldType : {vol, surface}
        Whether it is a volValueField or surfaceValueField
    verbose : bool
        Controls whether additional diagnostic data is written to the console.

    Returns
    -------
    FieldValue
        Contains the fieldvalue data.

    """
    timeDirs = np.array(os.listdir(path))
    timesFloat = [float(i) for i in timeDirs]
    timeDirs = timeDirs[np.argsort(timesFloat)]

    if timeDirs.size == 0:
        raise IOError
    elif verbose:
        print("Times: ", timeDirs)

    fieldValueData = []
    times = []
    for time in timeDirs:
        if os.path.isfile(join(path, time, fieldType+"FieldValue.dat")):
            dataI = FieldValue.read(join(path, time, fieldType+"FieldValue.dat"))
            times.append(dataI.times)
            fieldValueData.append(dataI.data)
            # The below should be the same for all time directories
            type = dataI.type
        elif verbose:
            print("No data for time ", time)

    if len(times) == 0:
        raise IOError("Field not found.")
    else:
        times = np.concatenate(times)
        times, ind = np.unique(times, return_index=True)
        data = np.row_stack(fieldValueData)[ind, :]

    return FieldValue(type, data, times)

