# This file is part of ofreaders
# (c) Timofey Mukha
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from os.path import join
import os
import numpy as np
import h5py

def collect_genfromtxt_over_time(path, filename, unique_col_idx=-1, **kwargs):
    """
    Collect collects data that can be read with numpy.genfromtxt over several time directories.

    Suitable for most data in a clean csv format. kwargs will be passed
    on to numpy.genfromtxt.

    Parameters
    ----------
    path : string
        The location of the folder holding the probe data. Typically
        in postProcessing.
    filename : string
        The name of the files holding the data
    unique_col_idx : int
        The index of the column that will be used to remove duplicates.
        Mostly useful if one of the columns is the time-stamp and
        duplicates should be removed.

    Returns
    -------
    ndarray
        Contains the data.

    """
    timeDirs = np.array(os.listdir(path))
    timesFloat = [float(i) for i in timeDirs]
    timeDirs = timeDirs[np.argsort(timesFloat)]

    if timeDirs.size == 0:
        raise IOError

    data = []
    for time in timeDirs:
        if os.path.isfile(join(path, time, filename)):
            data.append(np.genfromtxt(join(path, time, filename), **kwargs))

    data = np.row_stack(data)

    if unique_col_idx > -1:
        _, idx = np.unique(data[:, unique_col_idx], return_index=True)
        data = data[idx, :]

    if data.shape[0] == 0:
        raise IOError("Failed to read in the data")

    return data
