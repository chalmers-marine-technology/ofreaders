from setuptools import setup
from setuptools import find_packages

setup(name='ofreaders',
      version='0.0.1',
      description='Objects for reading and converting data written by OpenFOAM utilities',
      url='https://gitlab.com/chalmers-marine-technologies/ofreaders',
      author='Timofey Mukha',
      author_email='timofey@chalmers',
      packages=find_packages(),
      entry_points = {
          'console_scripts':[
                            ]
      },
      install_requires=[
                    'numpy',
                    'scipy',
                    'h5py'
                       ],
      license="MIT Licence",
      classifiers=[
          "Development Status :: 4 - Beta",
          "License :: OSI Approved :: MIT Licence"
      ],
      zip_safe=False)

