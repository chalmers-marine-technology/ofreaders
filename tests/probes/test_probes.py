# This file is part of ofreaders
# (c) Timofey Mukha, 2019
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

import pytest
import numpy as np
import h5py
from os.path import join
import os
from ofreaders import Probes, collect_probed_field_over_time
from numpy.testing import assert_allclose, assert_equal


def test_probes_init_scalar():
    probes = Probes.read(join("tests", "test_case", "postProcessing", "probesName", "31.6", "alpha.water"))

    assert (probes.n == 100)

    assert (probes.locations[-1, -1] == 0.24875)
    assert (probes.times[-1] == 31.6018)
    assert (probes.type == "scalar")
    assert (probes.data[0][2] == 3.47489e-06)

def test_probes_init_vector():
    probes = Probes.read(join("tests", "test_case", "postProcessing", "probesName", "31.6", "U"))

    assert (probes.n == 100)

    assert (probes.locations[-1, -1] == 0.24875)
    assert (probes.times[-1] == 31.6018)
    assert (probes.type == "vector")
    assert_allclose(probes.data[1][0][1], 0.0912694)

def test_probes_init_symmtensor():
    probes = Probes.read(join("tests", "test_case", "postProcessing", "probesName", "69.0004", "UPrime2Mean"))

    assert (probes.n == 100)

    assert (probes.times[-1] == 69.001)
    assert (probes.type == "symmTensor")
    assert_allclose(probes.data[-1][0][1], -8.39796e-05)

def test_probes_save_to_hdf5():
    probes = Probes.read(join("tests", "test_case", "postProcessing", "probesName", "31.6", "U"))
    path = join("tests", "test.hdf5")
    probes.save_to_hdf5(path)

    f = h5py.File(path)

    try:
        assert (f.attrs["n"] == probes.n)
        assert (f.attrs["type"] == probes.type)

        assert_allclose(f["locations"][:], probes.locations)
        assert_allclose(f["times"][:], probes.times)
        assert_allclose(f["data"][:], probes.data)
        f.close()
        os.remove(path)
    except Exception as e:
        f.close()
        os.remove(path)
        print(e)

def test_probe_over_time_no_times():
    """Assert IOError is raised if there are not time directories in the provided path"""
    with pytest.raises(IOError):
         collect_probed_field_over_time(join("tests", "test_case", "postProcessing", "probesKek"), "alpha.water")

def test_probe_over_time_no_field():
    """Assert IOError is raised when requested field not present"""
    with pytest.raises(IOError):
        collect_probed_field_over_time(join("tests", "test_case", "postProcessing", "probesName"), "alpha.kek")

def test_probe_over_time_scalar():
    """Test collecting  a scalar dataset"""
    collect_probed_field_over_time(join("tests", "test_case", "postProcessing", "probesName"), "alpha.water")

def test_probe_over_time_vector():
    """Test collecting a vector dataset"""
    collect_probed_field_over_time(join("tests", "test_case", "postProcessing", "probesName"), "U")

