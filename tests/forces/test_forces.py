# This file is part of ofreaders
# (c) Timofey Mukha, 2019
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

import pytest
import numpy as np
import h5py
from os.path import join
import os
from ofreaders import Forces, collect_forces_over_time, collect_moments_over_time
from numpy.testing import assert_allclose, assert_equal


def test_forces_read():
    """Test opening force.dat file"""
    forces = Forces.read(join("tests", "test_case", "postProcessing", "forcesName", "1.8", "force.dat"))

    assert(forces.data.shape == (9, 9))
    assert(forces.times.size == 9)
    assert(forces.times[0] == 1.8001)
    assert_allclose(forces.data[0, 0], 4.841101e+00)

def test_moments_read():
    """Test opening a momoment.dat file"""
    forces = Forces.read(join("tests", "test_case", "postProcessing", "forcesName", "1.8", "moment.dat"))

    assert(forces.data.shape == (9, 9))
    assert(forces.times.size == 9)
    assert(forces.times[0] == 1.8001)
    assert_allclose(forces.data[0, 0], -1.810350e+00)

def test_forces_components():
    """Test .p .nu and .tot"""
    forces = Forces.read(join("tests", "test_case", "postProcessing", "forcesName", "1.8", "force.dat"))

    assert(forces.p.shape == (9, 3))
    assert(forces.nu.shape == (9, 3))
    assert(forces.tot.shape == (9, 3))

def test_probes_save_to_hdf5():
    forces = Forces.read(join("tests", "test_case", "postProcessing", "forcesName", "1.8", "force.dat"))
    path = join("tests", "test.hdf5")
    forces.save_to_hdf5(path)

    f = h5py.File(path)

    try:
        assert_allclose(f["tot"][:], forces.tot)
        assert_allclose(f["p"][:], forces.p)
        assert_allclose(f["nu"][:], forces.nu)
        assert_allclose(f["times"][:], forces.times)
        f.close()
        os.remove(path)
    except Exception as e:
        f.close()
        os.remove(path)
        print(e)

def test_forces_over_time():
    """Test collecting over two files"""
    forces = collect_forces_over_time(join("tests", "test_case", "postProcessing", "forcesName"))

    assert(forces.times.size == 14)
    assert_allclose(forces.times[-1], 1.8014)
    assert_allclose(forces.p[-1, 0], 2.895995e+00)

def test_moments_over_time():
    """Test collecting over two files"""
    forces = collect_moments_over_time(join("tests", "test_case", "postProcessing", "forcesName"))

    assert(forces.times.size == 14)
    assert_allclose(forces.times[-1], 1.8014)
    assert_allclose(forces.p[-1, 0], -1.184385e+00)

def test_forces_over_time_no_times():
    """Assert IOError is raised if there are not time directories in the provided path"""
    with pytest.raises(IOError):
        collect_forces_over_time(join("tests", "test_case", "postProcessing", "forcesKek"))
