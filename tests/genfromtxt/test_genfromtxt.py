# This file is part of ofreaders
# (c) Timofey Mukha, 2020
# The code is released under the MIT Licence.
# See LICENCE.txt and the Legal section in the README for more information

import pytest
import numpy as np
import h5py
from os.path import join
from numpy.testing import assert_allclose, assert_equal
from ofreaders import collect_genfromtxt_over_time


def test_genfromtxt_over_time():
    data = collect_genfromtxt_over_time(join("tests", "test_case", "postProcessing", "genfromtxt"),
                                        "name")

    assert data.shape == (10, 2)
    assert_allclose(data[0, :], np.array([1.99997, 6.42538e-06]))

def test_duplicate_removal():
    data = collect_genfromtxt_over_time(join("tests", "test_case", "postProcessing", "genfromtxt"),
                                        "name", unique_col_idx=0)

    assert data.shape == (9, 2)
    assert_allclose(data[0, :], np.array([1.99997, 6.42538e-06]))

def test_passing_kwargs():
    data = collect_genfromtxt_over_time(join("tests", "test_case", "postProcessing", "genfromtxt"),
                                        "name", unique_col_idx=0, skip_header=3)

    assert data.shape == (6, 2)
